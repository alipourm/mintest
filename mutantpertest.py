import glob
import re
import sys



test = {}

for f in glob.glob(sys.argv[1]):
	# print f
	fcontent = open(f).read()
	mutresults = re.findall("(ts\d+.c[\.cmin]*:\d)", fcontent)
	# print mutresults, len(mutresults)
	# exit(0)
	for m in mutresults:
		m = m.strip()
		#  print m
		[tc, res_str] = m.split(':')
		res = int(res_str)
		if res == 1:
			f = f.split('/')[-1]
			if tc not in test:	
				test[tc] = [f]
			else:
				test[tc].append(f)


def compare(tc):
	mintest = tc + '.cmin'
	if mintest not in test:
		diff = len(test[tc])
	else: 
		diff = len(test[tc]) - len(test[mintest])

	return diff

# print '\documentclass{article} \\n \\begin{document} \n \\begin{tabular}{c|c|c | c}'

print 'num, TCName, OriginalMuKil, ReducedTCMuKill, Diff '
i = 0
for t in test:
	if 'cmin' not in t:
		diff = compare(t)
		i += 1
		print ('{4}, {0}, {1}, {2}, {3}').format(t, len(test[t]), len(test[t]) - diff, diff, i)

print '\end{document}'




