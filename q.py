#!/usr/bin/python

import os
import shutil
import subprocess
import time
import signal
import re
import getopt, sys
import glob
from optparse import OptionParser
from time import localtime, strftime
import string
import datetime
import itertools
from operator import or_
from sets import Set

folder = sys.argv[1]

mfiles=glob.glob('./%s/m*'%(folder))

skip = [line.strip() for line in open('skip')]

skiplist=[]
for sk in skip:
    isk=int(sk)
    skiplist.append('ts%06d.c'%(isk))
    skiplist.append('ts%06d.c.cmin'%(isk)) 

res=dict()
for mfile in mfiles:
   lines = [line.strip() for line in open(mfile)]
   assert(len(lines)==1)
   line=lines[0]
   parts=line.split(',')
   mutid=int(parts[0].split(':')[1])
   origkill=0
   minkill=0
   for i in range(1,len(parts)):
      s=parts[i]
      if(len(s)>0):
         segs=s.split(':')
         if( segs[1] == '1'):
            if( segs[0] not in skiplist):
               if( segs[0].find('cmin') >= 0):
                   minkill+=1
               else:
                   origkill+=1
   res[mutid]=(origkill,minkill)

f=open('count','w')
origkill=0
minkill=0
for r in res:
  f.write('%d,%d,%d\n'%(r, res[r][0], res[r][1]))
  if res[r][0] > 0:
    origkill+=1
  if res[r][1] > 0:
    minkill+=1
print('origkill=%d,minkill=%d\n'%(origkill,minkill))
f.close()
    


